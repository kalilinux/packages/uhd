uhd (3.5.3+git20130814-1kali0) kali; urgency=low

  * Imported new upstream release  (Closes: 0000506)

 -- Devon Kearns <dookie@kali.org>  Wed, 14 Aug 2013 10:42:39 -0600

uhd (3.4.2-1) unstable; urgency=low

  * New upstream release
    * USRP2/N2x0:
          o Card and net burner language fixes
          o Net burner python v3 code fix
          o Net burner IPv6 interface fix
    * E1x0:
          o Fix for FPGA timing issue with GPMC input
          o Incremented FPGA compat number to 9.2
    * B100:
          o Fix USB wrapper/buffer release race condition
    * USRP1:
          o Fix DAC calculation for tune out of 1st nyquist zone
    * General:
          o Fix for recv packet handler time error check
          o SIMD conversion routines priority over table look-up
          o Fix undefined GCC float conversion behaviour for sc8

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 23 May 2012 20:50:59 -0400

uhd (3.4.1-1) unstable; urgency=low

  * New upstream release
      *  USRP2/N2x0:
          o Filter out invalid broadcast replies
          o Incremented FPGA compat number to 9.1
    * E1x0:
          o Incremented FPGA compat number to 9.1
    * B100:
          o FPGA fixes for USB slave FIFO interface
          o Incremented FPGA compat number to 9.3
    * USRP1:
          o Stop thread in deconstructor for race condition
          o Fixed DBSRX + USRP1 i2c lockup condition
    * Gen2:
          o Fix for unintentional clear in deprecated recv() call
          o Fix RX DC offset call to handle negative values
    * FreeBSD:
          o Fixed network relay example compilation

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 21 Apr 2012 21:40:35 -0400

uhd (3.4.0-3) unstable; urgency=low

  * include upstream fix bsd compilation for network relay example
    (Closes: #667079)
  * use liborc again, revert 3.4.0-2 changes.

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 04 Apr 2012 22:43:26 -0400

uhd (3.4.0-2) unstable; urgency=low

  * Build without liborc to hunt bugs in convert_test

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 03 Apr 2012 11:43:02 -0400

uhd (3.4.0-1) unstable; urgency=low

  * New upstream release
    *  USRP2/N2x0:
            o 50 Msps RX/TX with sc8 mode over the wire
      * B100:
            o 16 Msps RX/TX with sc8 mode over the wire
      * SBX/WBX:
            o Added self-calibration utilities
      * Gen2:
            o Control RX/TX DC offset correction via API
            o Control RX/TX IQ balance correction via API
            o Incremented FPGA compat number to 9
      * USRP1:
            o Support 16Msps RX with sc8 mode over the wire
            o Control RX DC offset correction via API
      * Misc:
            o Multiple streamers/heterogeneous rates
            o Alternative host and wire data types
            o Added API calls for DC offset correction
            o Added API calls for IQ balance correction
  * Improved description (Closes: #658355)
  
 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 23 Mar 2012 08:34:48 -0400

uhd (3.3.2-3) unstable; urgency=low

  * more robust postinst (Closes: #656119)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 17 Jan 2012 11:05:15 -0500

uhd (3.3.2-2) unstable; urgency=low

  * improved uhd-host package
    - include manual documentation
    - configure sysctl and pam limits settings
  * Add README.Debian
  * upload to unstable

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 11 Jan 2012 13:37:13 -0500

uhd (3.3.2-1) experimental; urgency=low

  * New upstream release
  * Packaged for Debian (Closes: #644789)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 10 Jan 2012 00:18:49 -0500

uhd (3.3.1-1) experimental; urgency=low

  * Keep up, rebase package with release_003_003_001 tag

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 12 Nov 2011 20:00:49 -0500

uhd (3.2.4-1) unstable; urgency=low

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 07 Oct 2011 21:45:37 -0400

uhd (3.2.3-1) experimental; urgency=low

  * Keep up, rebase package with release_003_002_003 tag

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 20 Sep 2011 12:17:13 -0400

uhd (3.2.1-1) experimental; urgency=low

  * Package from upstream git

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri,  5 Aug 2011 19:12:07 -0500
